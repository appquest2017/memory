package ch.appquest.memory.model;

import android.graphics.Bitmap;

/**
 * @author Janick Lehmann
 */

public class MemoryCard {
    String text;
    Bitmap img;

    public MemoryCard(String text, Bitmap img) {
        this.text = text;
        this.img = img;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap imgId) {
        this.img = img;
    }
}
