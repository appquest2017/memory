package ch.appquest.memory;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ch.appquest.memory.lib.BitmapAddOn;
import ch.appquest.memory.lib.SaveAndLoad;
import ch.appquest.memory.model.MemoryCard;
import ch.appquest.memory.util.Backup;
import ch.appquest.memory.util.DragAndDrop;
import ch.appquest.memory.util.ItemClickListener;
import ch.appquest.memory.util.ItemLongClickListener;
import ch.appquest.memory.util.RVAdapter;

public class MainActivity extends AppCompatActivity implements ItemClickListener, ItemLongClickListener {
    private RecyclerView rv;
    private FloatingActionButton fab;
    private ProgressBar progress;
    private RelativeLayout root;
    private TextView lblProgress;

    private RVAdapter adapter;
    private List<MemoryCard> memoryCards;
    private int currentPosition = -1;
    private Bitmap default_image = null;

    private void initAdapter() {
        adapter = new RVAdapter(memoryCards);
        adapter.setClickListener(this, this);
        rv.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new DragAndDrop());
        itemTouchHelper.attachToRecyclerView(rv);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        default_image = BitmapFactory.decodeResource(getResources(), R.drawable.camera);

        rv = (RecyclerView) findViewById(R.id.recyclerView);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        progress = (ProgressBar) findViewById(R.id.progressBar);
        root = (RelativeLayout) findViewById(R.id.root);
        lblProgress = (TextView) findViewById(R.id.lblProgress);

        // Layout Recycler View
        rv.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        rv.setLayoutManager(gridLayoutManager);

        memoryCards = new ArrayList<>();
        initAdapter();

        new Restore().execute(this);

        listener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        backup();
    }

    private void backup() {
        JSONObject json = new JSONObject();
        try {
            json.put("data", getFullJSONFromView());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        SaveAndLoad.saveData("data.tmp", json.toString(), this);
    }

    public void listener() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter.add(new MemoryCard("Placeholder " + (memoryCards.size() + 1), default_image));
            }
        });
    }

    public void takeQrCodePicture() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setCaptureActivity(MyCaptureActivity.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES);
        integrator.setOrientationLocked(false);
        integrator.addExtra(Intents.Scan.BARCODE_IMAGE_ENABLED, true);
        integrator.initiateScan();
    }

    @Override
    public void onClick(View view, int position) {
        Toast.makeText(this, "Position: " + position, Toast.LENGTH_LONG).show();
        currentPosition = position;
        takeQrCodePicture();
    }

    @Override
    public void onLongClick(View view, final int position) {
        AlertDialog.Builder builder = null;
        builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete item")
                .setMessage("Are you sure you want to delete this item?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.remove(position);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(R.drawable.ic_delete_black_24dp)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentIntegrator.REQUEST_CODE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            String path = extras.getString(Intents.Scan.RESULT_BARCODE_IMAGE_PATH);
            String code = extras.getString(Intents.Scan.RESULT);

            if (currentPosition >= 0) {
                adapter.update(currentPosition, new MemoryCard(code, BitmapFactory.decodeFile(path)));
            } else {
                adapter.add(new MemoryCard(code, BitmapFactory.decodeFile(path)));
            }
            //Toast.makeText(this, path, Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.submit, menu);
        MenuItem send = menu.findItem(R.id.send);
        MenuItem save = menu.findItem(R.id.save);

        send.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                log();
                return false;
            }
        });
        save.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                backup();
                Snackbar.make(root, "Saved", Snackbar.LENGTH_LONG).show();
                return false;
            }
        });

        return true;
    }

    //Includes Images and Text
    private JSONArray getFullJSONFromView() throws JSONException {
        int i = 0;
        String paired_word = "", bitmap = "";

        JSONArray json = new JSONArray();
        for (MemoryCard curr : memoryCards) {
            if (i % 2 == 0) {
                paired_word = curr.getText();
                bitmap = BitmapAddOn.BitMapToString(BitmapAddOn.compress(curr.getImg()));
            } else {
                JSONArray curr_json = new JSONArray();
                JSONObject inner_json_1 = new JSONObject(), inner_json_2 = new JSONObject();

                inner_json_1.put("word", paired_word);
                inner_json_1.put("img", bitmap);

                inner_json_2.put("word", curr.getText());
                inner_json_2.put("img", BitmapAddOn.BitMapToString(BitmapAddOn.compress(curr.getImg())));

                curr_json.put(inner_json_1);
                curr_json.put(inner_json_2);

                json.put(curr_json);
                paired_word = "";
                bitmap = "";
            }
            i++;
        }
        return json;
    }

    //Includes just MemoryText
    private JSONArray getJSONFromView() {
        int i = 0;
        String paired_word = "";

        JSONArray json = new JSONArray();
        for (MemoryCard curr : memoryCards) {
            if (i % 2 == 0) {
                paired_word = curr.getText();
            } else {
                JSONArray curr_json = new JSONArray();
                curr_json.put(paired_word);
                curr_json.put(curr.getText());
                json.put(curr_json);
                paired_word = "";
            }
            i++;
        }
        return json;
    }

    private void log() {
        if (memoryCards.size() % 2 == 0) { //Gibt es immer zwei Paare
            Intent intent = new Intent("ch.appquest.intent.LOG");

            if (getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
                Toast.makeText(this, "Logbook App not Installed", Toast.LENGTH_LONG).show();
                return;
            }

            try {
                // Create JSON
                JSONObject jsonResult = new JSONObject();
                jsonResult.put("task", "Memory");
                jsonResult.put("solution", getJSONFromView());
                intent.putExtra("ch.appquest.logmessage", jsonResult.toString());
            } catch (JSONException ex) {
                Toast.makeText(this, "Error while creating JSON", Toast.LENGTH_LONG).show();
                return;
            }

            startActivity(intent);
        } else {
            Toast.makeText(this, "Please create always pairs.", Toast.LENGTH_SHORT).show();
        }
    }

    class Restore extends AsyncTask<Context, Void, Boolean> {

        private String error_message = "";

        @Override
        protected Boolean doInBackground(Context... strings) {
            try {
                if(getFileStreamPath("data.tmp").exists()) {
                    Backup.restore(adapter, strings[0]); //Get old data
                }
                return true;
            } catch (Exception ex) {
                error_message = ex.getLocalizedMessage();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                Snackbar.make(root, "Restore finished", Snackbar.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
                lblProgress.setVisibility(View.GONE);
            } else {
                Toast.makeText(MainActivity.this, error_message, Toast.LENGTH_SHORT).show();
                Snackbar.make(root, "Restore finished with failures", Snackbar.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
                lblProgress.setVisibility(View.GONE);
            }
        }
    }
}
