package ch.appquest.memory.util;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ch.appquest.memory.R;
import ch.appquest.memory.model.MemoryCard;

/**
 * @author Janick Lehmann
 */

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.MemoryCardViewHolder> {
    private List<MemoryCard> memoryCards;
    private ItemClickListener clickListener;
    private ItemLongClickListener longClickListener;

    public RVAdapter(List<MemoryCard> memoryCards) {
        this.memoryCards = memoryCards;
    }

    @Override
    public MemoryCardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.memorycard_listitem, parent, false);
        return new MemoryCardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MemoryCardViewHolder holder, int position) {
        holder.textView.setText(memoryCards.get(position).getText());
        holder.imageView.setImageBitmap(memoryCards.get(position).getImg());
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return memoryCards.size();
    }

    public void setClickListener(ItemClickListener clickListener, ItemLongClickListener longClickListener) {
        this.clickListener = clickListener;
        this.longClickListener = longClickListener;
    }

    public void add(MemoryCard item) {
        memoryCards.add(item);
        notifyItemInserted(getItemCount() - 1);
    }

    public void update(int position, MemoryCard item) {
        memoryCards.set(position, item);
        notifyItemChanged(position);
    }

    public void remove(int position) {
        memoryCards.remove(position);
        notifyItemRemoved(position);
    }

    public void move(int from, int to) {
        MemoryCard memoryCard = memoryCards.get(from);
        memoryCards.remove(from);
        if (to == -1) {
            memoryCards.add(memoryCard);
            notifyItemMoved(from, getItemCount() - 1);
        } else {
            memoryCards.add(to, memoryCard);
            notifyItemMoved(from, to);
        }
    }

    class MemoryCardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        CardView cardView;
        ImageView imageView;
        TextView textView;
        ImageButton btnMoveDown;

        MemoryCardViewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.cardView);
            imageView = itemView.findViewById(R.id.imageView);
            textView = itemView.findViewById(R.id.textView);
            btnMoveDown = itemView.findViewById(R.id.btnMoveDown);

            itemView.setOnClickListener(this);
            btnMoveDown.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (clickListener != null)
                clickListener.onClick(view, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            if (longClickListener != null)
                longClickListener.onLongClick(view, getAdapterPosition());
            return false;
        }
    }
}
