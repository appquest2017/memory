package ch.appquest.memory.util;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ch.appquest.memory.lib.BitmapAddOn;
import ch.appquest.memory.lib.SaveAndLoad;
import ch.appquest.memory.model.MemoryCard;

public class Backup {

    public static void restore(RVAdapter adapter, Context context) {
        String data = SaveAndLoad.loadData("data.tmp", context);

        System.out.println(data);

        JSONObject json = null;
        try {
            json = new JSONObject(data);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JSONArray json_data_array = null;
        try {
            json_data_array = json.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < json_data_array.length(); i++) {
            try {
                JSONArray curr_array = json_data_array.getJSONArray(i);
                for (int ia = 0; ia < curr_array.length(); ia++) {
                    JSONObject curr = curr_array.getJSONObject(ia);
                    adapter.add(new MemoryCard(curr.getString("word"), BitmapAddOn.StringToBitMap(curr.getString("img"))));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
