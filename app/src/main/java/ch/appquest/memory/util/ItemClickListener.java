package ch.appquest.memory.util;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
